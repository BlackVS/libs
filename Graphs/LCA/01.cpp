// kevinsogo
// https://www.hackerrank.com/contests/world-codesprint-13/challenges/landslide/editorial
#include <bits/stdc++.h>
using namespace std;

typedef pair<int,int> edge;

int flg(int n) {
    return n <= 1 ? 0 : 1 + flg(n >> 1);
}

class Fen {
    vector<int> a;
public:
    Fen(int n): a(n + 1, 0) {}
    void inc(int i, int v) {
        for (i++; i < a.size(); i += i & -i) a[i] += v;
    }
    int sum(int i) {
        int v = 0;
        for (i++; i > 0; i -= i & -i) v += a[i];
        return v;
    }
};

class Tree {
    int n, kmax;
    vector<vector<int>> adj, anc;
    vector<int> parent, depth, prei, prej;
    Fen dst;
    vector<bool> destroyed;
public:
    Tree(int n): n(n), kmax(flg(n) + 1),
            adj(n), anc(kmax, vector<int>(n)),
            parent(n, -1), depth(n), prei(n), prej(n),
            dst(n), destroyed(n) {}
    void add_edge(int a, int b) {
        adj[a].push_back(b);
        adj[b].push_back(a);
    }
    void prepare() {
        parent[0] = 0;
        vector<int> stack(1, 0), pre;
        while (!stack.empty()) {
            int i = stack.back(); stack.pop_back();
            prej[i] = 1 + (prei[i] = pre.size());
            pre.push_back(i);
            for (int j : adj[i]) {
                if (!~parent[j]) {
                    parent[j] = i;
                    depth[j] = depth[i] + 1;
                    stack.push_back(j);
                }
            }
        }
        reverse(pre.begin(), pre.end());
        pre.pop_back();
        for (int i : pre) {
            prej[parent[i]] = max(prej[parent[i]], prej[i]);
        }
        for (int i = 0; i < n; i++) anc[0][i] = parent[i];
        for (int k = 0; k < kmax - 1; k++) {
            for (int i = 0; i < n; i++) anc[k + 1][i] = anc[k][anc[k][i]];
        }
    }
    int ascend(int i, int d) {
        for (int k = 0; d > 0; d >>= 1, k++) {
            if (d & 1) i = anc[k][i];
        }
        return i;
    }
    int lca(int a, int b) {
        if (depth[a] > depth[b]) a = ascend(a, depth[a] - depth[b]);
        if (depth[b] > depth[a]) b = ascend(b, depth[b] - depth[a]);
        if (a == b) return a;
        for (int k = kmax; k --> 0;) {
            if (anc[k][a] != anc[k][b]) {
                a = anc[k][a];
                b = anc[k][b];
            }
        }
        return parent[a];
    }
    void add(int i, int d) {
        dst.inc(prei[i], +d);
        dst.inc(prej[i], -d);
    }
    int dest(int i) {
        return dst.sum(prei[i]);
    }
    int ask(int a, int b) {
        int l = lca(a, b);
        return dest(a) + dest(b) == 2*dest(l) ? depth[a] + depth[b] - 2*depth[l] : -1;
    }
    void destroy(int a, int b) {
        if (parent[a] != b) swap(a, b);
        if (parent[a] == b && !destroyed[a]) {
            destroyed[a] = true;
            add(a, +1);
        }
    }
    void undestroy(int a, int b) {
        if (parent[a] != b) swap(a, b);
        if (parent[a] == b && destroyed[a]) {
            destroyed[a] = false;
            add(a, -1);
        }
    }
};

int main() {
    int n;
    scanf("%d", &n);
    Tree t(n);
    for (int i = 1; i < n; i++) {
        int a, b;
        scanf("%d%d", &a, &b);
        a--, b--;
        t.add_edge(a, b);
    }
    t.prepare();

    int q;
    scanf("%d", &q);
    while (q--) {
        char typ;
        int a, b;
        scanf(" %c%d%d", &typ, &a, &b);
        a--, b--;
        if (typ == 'd') {
            t.destroy(a, b);
        } else if (typ == 'c') {
            t.undestroy(a, b);
        } else { // 'q'
            int res = t.ask(a, b);
            if (res == -1) {
                puts("Impossible");
            } else {
                printf("%d\n", res);
            }
        }
    }
}