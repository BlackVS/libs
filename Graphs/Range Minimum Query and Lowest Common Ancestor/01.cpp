// https://www.hackerrank.com/contests/world-codesprint-13/challenges/landslide/editorial

#include <bits/stdc++.h>
using namespace std;
#define pii pair<int,int>
#define ff first
#define ss second
#define mod 1000000007
#define mod2 1000000009

struct node
{
	int ff,ss;
	int ff_p,ss_p;
	node(){};
	node(int a,int b=0,int c=0,int d=0)
	{
		ff=a;
		ss=b;
		ff_p=c;
		ss_p=d;
	}
	void update(pii a)
	{
		ff ^= a.ff;
		ss ^= a.ss;
		ff_p ^= a.ff;
		ss_p ^= a.ss;
	}
	void update(node a)
	{
		ff ^= a.ff_p;
		ss ^= a.ss_p;
		ff_p ^= a.ff_p;
		ss_p ^= a.ss_p;
	}
	void clear()
	{
		ff_p = 0;
		ss_p = 0;
	}
	bool operator==(node a)
	{
		return ff == a.ff && ss == a.ss;
	}
};

#define N 200005
#define hash hashh
int depth[N],in[N],out[N],moment[3*N],n,x,y,q,T,act,pas,tree[4*3*N];
node hash[4*3*N];
char s[10];
set<int> v[N];
pii storedHash[N];

void dfs(int id,int par,int d)
{
	depth[id] = d;
	in[id] = ++T;
	moment[T] = id;

	for(auto u : v[id])
	{
		if( u == par )
			continue;

		dfs(u,id,d+1);

		moment[++T] = id;
	}

	out[id] = T;
	moment[T] = id;
}

void build(int idx,int l,int r) // tree is for path queries, hash is for connectivity queries
{
	if( l == r )
	{
		tree[idx] = moment[r]; // vertex at moment r
		hash[idx].update(node(act,pas));
		return;
	}

	int mid = ( l + r ) / 2;

	build(2*idx,l,mid);
	build(2*idx+1,mid+1,r);

	tree[idx] = tree[2*idx];

	if( depth[ tree[idx] ] > depth[ tree[2*idx+1] ] )
		tree[idx] = tree[2*idx+1];
}

inline int getr()
{
	return rand()%mod;
}

inline int getr2()
{
	return rand()%mod2;
}

void init()
{
	dfs(1,0,1);

	act = 1ll * getr() * getr() % mod;
	pas = 1ll * getr2() * getr2() % mod2;

	build(1,1,T);
}

int query(int idx,int lup,int rup,int l,int r)
{
	if( lup <= l && r <= rup )
		return tree[idx];

	if( l > rup || lup > r )
		return -1;

	int mid = ( l + r ) / 2;

	int p = query(2*idx,lup,rup,l,mid);
	int q = query(2*idx+1,lup,rup,mid+1,r);

	if( p == -1 ) return q;
	if( q == -1 ) return p;

	if( depth[p] < depth[q] ) return p;
	return q;
}

inline int lca(int x,int y)
{
	if( x == y )
		return x;

	if( depth[y] < depth[x] )
		return lca(y,x);

	// x is higher than y

	if( in[x] < in[y] && out[y] < out[x] )
		return x;

	if( in[x] < in[y] )
		return query(1,out[x]+1,in[y]-1,1,T);

	return query(1,out[y]+1,in[x]-1,1,T);
}

inline void lazy(int idx)
{
	hash[2*idx].update(hash[idx]);
	hash[2*idx+1].update(hash[idx]);
	hash[idx].clear();
}

node getHash(int idx,int lup,int rup,int l,int r)
{
	if( lup <= l && r <= rup )
		return hash[idx];

	if( l > rup || lup > r )
		return node(-1,-1);

	int mid = ( l + r ) / 2;

	lazy(idx);

	node p = getHash(2*idx,lup,rup,l,mid);
	node q = getHash(2*idx+1,lup,rup,mid+1,r);

	if( p.ff != -1 )
		return p;
	return q;
}

void shield(int idx,int lup,int rup,pii val,int l,int r)
{
	if( lup <= l && r <= rup )
	{
		hash[idx].update(val);
		return;
	}

	if( l > rup || lup > r )
		return;

	int mid = ( l + r ) / 2;

	lazy(idx);

	shield(2*idx,lup,rup,val,l,mid);
	shield(2*idx+1,lup,rup,val,mid+1,r);
}

inline bool sameComponent(int x,int y)
{
	return getHash(1,in[x],in[x],1,T) == getHash(1,in[y],in[y],1,T);
}

inline void cut(int x,int y)
{
	if( depth[y] < depth[x] )
		return cut(y,x);

	int val = 1ll * getr() * getr() % mod;
	int val2 = 1ll * getr2() * getr2() % mod2;

	storedHash[y] = make_pair(val,val2);

	shield(1,in[y],out[y],storedHash[y],1,T);
}

inline void join(int x,int y)
{
	if( depth[y] < depth[x] )
		return join(y,x);

	shield(1,in[y],out[y],storedHash[y],1,T);
}

int main() 
{
	ios_base::sync_with_stdio(false);cin.tie(NULL);
	cin >> n;
	for(int i=1;i<n;i++)
	{
		cin >> x >> y;
		v[x].insert(y);
		v[y].insert(x);
	}
	
	init();

	cin >> q;

	while(q--)
	{
		cin >> s;

		if( s[0] == 'd' ) // disconnect
		{
			cin >> x >> y;

			if( v[x].find(y) == v[x].end() || !sameComponent(x,y) )
				continue;

			cut(x,y);
		}

		if( s[0] == 'c' ) // cleared
		{
			cin >> x >> y;

			if( v[x].find(y) == v[x].end() || sameComponent(x,y) )
				continue;

			join(x,y);
		}

		if( s[0] == 'q' ) // query
		{
			cin >> x >> y;

			if( !sameComponent(x,y) )
				cout << "Impossible" << endl;
			else
			{
				int top = lca(x,y);
				cout << depth[x] + depth[y] - 2 * depth[top] << endl;
			}
		}
	}

	return 0;
}