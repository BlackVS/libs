#include <iostream>
#include <cstdio>
#include <string>
#include <sstream> 
#include <vector>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <ctime>
#include <cassert>
using namespace std;
#define pb push_back
#define mp make_pair
#define pii pair<int,int>
#define fi first
#define se second
#define vi vector<int>
#define vpii vector<pii>
#define SZ(x) ((int)(x.size()))
#define info(vari) cerr << #vari<< " = "<< (vari) <<endl;

#define assertRange(x,a,b) assert((a) <= (x) and (x) <= (b))
typedef long long ll;
const int INF = 1e9;

const int N = 2e5;
const int Q = 2e5;

const int L = 18; // for LCA sparse table size
const int M = 1<<19; // for segment tree size
const ll B = 3e5+7; //prime greater than alphabet

vector<int> g[N];

const int MOD = 1e9+7;

int d[N];
void dfsDistances(int v, int p) {
    stack<pair<pii, int>> q;
    q.push(mp(mp(v,p),0));
    while (!q.empty()) {
        int v = q.top().fi.fi;
        int p = q.top().fi.se;
        int dst = q.top().se;
        q.pop();
        d[v] = dst;
        for (auto u : g[v]) {
            if (u == p) continue;
            q.push(mp(mp(u, v), dst+1));
        }
    }
}

namespace EulerTour {
    int enterTime[N];
    int exitTime[N];
    int curTime = 0;

    void dfs(int v, int p) {
        stack<pair<int, pii>> q;
        q.push(mp(0, mp(v,p)));
        while (!q.empty()) {
            int eventType = q.top().fi;
            int v = q.top().se.fi;
            int p = q.top().se.se;
            q.pop();
            if (eventType == 0) {
                enterTime[v] = curTime;
                ++curTime;
                q.push(mp(1, mp(v, p)));
                for (auto u : g[v]) {
                    if (u == p) continue;
                    q.push(mp(0, mp(u, v)));
                }
            } else {
                exitTime[v] = curTime;
                ++curTime;
            }
        }
    }
};

namespace LCA {
    int par[N];
    int dp[N][L];

    void dfsParent(int v, int p) {
        stack<pii> q;
        q.push(mp(v,p));
        while (!q.empty()) {
            int v = q.top().fi;
            int p = q.top().se;
            q.pop();
            par[v] = p;
            for (auto u : g[v]) {
                if (u == p) continue;
                q.push(mp(u, v));
            }
        }
    }

    void precompute(int root, int n) {
        dfsParent(root, -1);
        for (int i = 0; i < n; ++i) {
            dp[i][0] = par[i];
        }
        for (int k = 1; k < L; ++k) {
            for (int i = 0; i < n; ++i) {
                if (dp[i][k-1] != -1) {
                    dp[i][k] = dp[dp[i][k-1]][k-1];
                } else {
                    dp[i][k] = -1;
                }
            }
        }
    }

    int query(int v, int u) {
        if (d[v] < d[u]) swap(v,u);
        for (int k = L-1; k >= 0 and d[v] > d[u]; --k) {
            if (d[v] - (1 << k) >= d[u]) {
                v = dp[v][k];
            }
        }
        assert(d[v] == d[u]);
        if (v == u) return v;
        for (int k = L-1; k >= 0; --k) {
            if (dp[v][k] != dp[u][k]) {
                v = dp[v][k];
                u = dp[u][k];
                assert(d[v] == d[u]);
            }
        }
        return dp[v][0];
    }
};

namespace SegmentTree {
    ll t[2*M];

    ll query(int x) {
        x += M;
        ll res = t[x];
        while (x > 1) {
            x /= 2;
            res += t[x];
            res %= MOD;
        }
        return res;
    }

    void add(int x, int y, ll val) {
        x += M;
        y += M;
        t[x] += val;
        t[x] %= MOD;
        if (x == y) return;
        t[y] += val;
        t[y] %= MOD;
        while (x/2 != y/2) {
            if (x % 2 == 0) {
                t[x+1] += val;
                t[x+1] %= MOD;
            }
            if (y % 2 == 1) {
                t[y-1] += val;
                t[y-1] %= MOD;
            }
            x /= 2;
            y /= 2;
        }
    }
};


ll pp[N+1];
bool damaged[N];

int main() {
    ios_base::sync_with_stdio(0);
    int n;
    cin >> n;
    assertRange(n, 1, N);
    pp[0] = 1;
    for (int i = 1; i <= n; ++i) {
        pp[i] = pp[i-1] * B;
        pp[i] %= MOD;
    }
    map<pii, int> edgeToId;
    for (int i = 0; i < n-1; ++i) {
        int v, u;
        cin >> v >> u;
        assertRange(v, 1, n);
        assertRange(u, 1, n);
        --v, --u;
        if (v > u) swap(v,u);
        g[v].pb(u);
        g[u].pb(v);
        edgeToId[mp(v,u)] = i+1;
    }
    const int ROOT = 0;
    dfsDistances(ROOT, -1);
    EulerTour::dfs(ROOT, -1);
    LCA::precompute(ROOT, n);
    int q; 
    cin >> q;
    assertRange(q, 1, Q);
    for (int qid = 0; qid < q; ++qid) {
        char qtype;
        cin >> qtype;
        assert(qtype == 'd' or qtype == 'c' or qtype == 'q');
        int v, u;
        cin >> v >> u;
        assertRange(v, 1, n);
        assertRange(u, 1, n);
        --v, --u;
        if (v > u) swap(v,u);
        auto e = mp(v,u);
        if (qtype == 'd') {
            auto it = edgeToId.find(e);
            if (it == edgeToId.end()) continue;
            int edgeId = it->se;
            if (damaged[edgeId]) continue;
            if (d[v] < d[u]) swap(v,u);
            SegmentTree::add(EulerTour::enterTime[v], EulerTour::exitTime[v], pp[edgeId]);
            damaged[edgeId] = 1;
        } else if (qtype == 'c') {
            auto it = edgeToId.find(e);
            if (it == edgeToId.end()) continue;
            int edgeId = it->se;
            if (!damaged[edgeId]) continue;
            if (d[v] < d[u]) swap(v,u);
            SegmentTree::add(EulerTour::enterTime[v], EulerTour::exitTime[v], -pp[edgeId]+MOD);
            damaged[edgeId] = 0;
        } else {
            ll hv = SegmentTree::query(EulerTour::enterTime[v]);
            ll hu = SegmentTree::query(EulerTour::enterTime[u]);
            if (hv != hu) {
                cout << "Impossible" << endl;
                continue;
            }
            cout << d[v] + d[u] - 2 * d[LCA::query(v,u)] << endl;
        }
    }
    return 0;
}