#!/bin/python3
import sys, copy, itertools
from collections import *
#input = stdin.readline

fstd="t1"
sys.stdin = open('..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\%s.out' % fstd , 'w+')

# vertices have nums 0..N-1
class Graph(object):
    def __init__(self,n):
        self.VN=n
        self.G = [[] for _ in range(n)]
        self.VDegree = [0]*n
        # DFS
        self.DFS_NR  = None
        self.DFS_PR  = None
        #SubGraphs
        self.SB_P=None
        self.SB_R=None
        self.SB_L=None
        self.SB_N=None

    def __str__(self,shortform=True):
        if shortform:
            return str(self.G)
        res = "vertices: " + str(self.VN)
        res += "\nedges:\n"
        for i,x in enumerate(self.G):
            for y in x:
                res += str(i) + " -> " + str(y) + "\n"
        return res

    #split into separate components
    #main idea - each subgraph store as sequence of vertecis in order of finding to speed up traverse
    #for each subgraph keep beginning and end of this sequence (root and last)
    #for each vertice keep it's parent in sequence
    #in such case joining of subgraphs means just corrrecting few values       
    def merge(self,v1,v2):
        N=self.VN
        #init
        if not self.SB_P:
            self.SB_P=[None]*N
            self.SB_R=list(range(N))
            self.SB_L=list(range(N))
            self.SB_N=N
        #vertex parent
        P=self.SB_P
        #subgraph root
        R=self.SB_R
        #subgraph end
        L=self.SB_L
        r1 = R[v1]
        r2 = R[v2]
        if r1 == r2:
            #already in one subgraph
            return
        #join subgraphs
        r1, r2=min(r1,r2),max(r1,r2)
        P[r2]=L[r1]
        L[r1]=L[r2]
        v=L[r1]
        while R[v]==r2:
            R[v]=r1
            v=P[v]
        self.SB_N-=1

    def isSameSubgraph(self,U,V):
        if not self.SB_R:
            return None
        return self.SB_R[U]==self.SB_R[V]


    def add_edge(self,x,y,directed=False,fmerge=True):
        self.G[x].append(y)
        self.VDegree[x]+=1
        if not directed:
            self.G[y].append(x)
            self.VDegree[y]+=1
        if fmerge:
            self.merge(x,y)



    def SCC1(self):

        """
        http://www.logarithmic.net/pfh-files/blog/01208083168/tarjan.py

        Tarjan's Algorithm (named for its discoverer, Robert Tarjan) is a graph theory algorithm
        for finding the strongly connected components of a graph.
    
        Based on: http://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
        """

        index_counter = [0]
        stack = []
        lowlinks = {}
        index = {}
        result = []
        graph=self.G

        def strongconnect(node):
            # set the depth index for this node to the smallest unused index
            index[node] = index_counter[0]
            lowlinks[node] = index_counter[0]
            index_counter[0] += 1
            stack.append(node)
    
            # Consider successors of `node`
            try:
                successors = graph[node]
            except:
                successors = []
            for successor in successors:
                if successor not in lowlinks:
                    # Successor has not yet been visited; recurse on it
                    strongconnect(successor)
                    lowlinks[node] = min(lowlinks[node],lowlinks[successor])
                elif successor in stack:
                    # the successor is in the stack and hence in the current strongly connected component (SCC)
                    lowlinks[node] = min(lowlinks[node],index[successor])
        
            # If `node` is a root node, pop the stack and generate an SCC
            if lowlinks[node] == index[node]:
                connected_component = []
            
                while True:
                    successor = stack.pop()
                    connected_component.append(successor)
                    if successor == node: break
                component = tuple(connected_component)
                # storing the result
                result.append(component)
    
        for node in range(self.VN):
            if node not in lowlinks:
                strongconnect(node)
    
        return result          


    def SCC2(self):
        """
        https://www.hackerrank.com/rest/contests/world-codesprint-11/challenges/hackerland/hackers/thegwtg/download_solution

        This is a non-recursive version of strongly_connected_components_path.
        See the docstring of that function for more details.

        Examples
        --------
        Example from Gabow's paper [1]_.

        >>> vertices = [1, 2, 3, 4, 5, 6]
        >>> edges = {1: [2, 3], 2: [3, 4], 3: [], 4: [3, 5], 5: [2, 6], 6: [3, 4]}
        >>> for scc in strongly_connected_components_iterative(vertices, edges):
        ...     print(scc)
        ...
        set([3])
        set([2, 4, 5, 6])
        set([1])

        Example from Tarjan's paper [2]_.

        >>> vertices = [1, 2, 3, 4, 5, 6, 7, 8]
        >>> edges = {1: [2], 2: [3, 8], 3: [4, 7], 4: [5],
        ...          5: [3, 6], 6: [], 7: [4, 6], 8: [1, 7]}
        >>> for scc in  strongly_connected_components_iterative(vertices, edges):
        ...     print(scc)
        ...
        set([6])
        set([3, 4, 5, 7])
        set([8, 1, 2])

        """
        identified = set()
        stack = []
        index = {}
        boundaries = []
        graph = self.G

        for v in range(self.VN):
            if v not in index:
                to_do = [('VISIT', v)]
                while to_do:
                    operation_type, v = to_do.pop()
                    if operation_type == 'VISIT':
                        index[v] = len(stack)
                        stack.append(v)
                        boundaries.append(index[v])
                        to_do.append(('POSTVISIT', v))
                        # We reverse to keep the search order identical to that of
                        # the recursive code;  the reversal is not necessary for
                        # correctness, and can be omitted.
                        to_do.extend(
                            reversed([('VISITEDGE', w) for w in graph[v]]))
                    elif operation_type == 'VISITEDGE':
                        if v not in index:
                            to_do.append(('VISIT', v))
                        elif v not in identified:
                            while index[v] < boundaries[-1]:
                                boundaries.pop()
                    else:
                        # operation_type == 'POSTVISIT'
                        if boundaries[-1] == index[v]:
                            boundaries.pop()
                            scc = set(stack[index[v]:])
                            del stack[index[v]:]
                            identified.update(scc)
                            yield scc


class TarjanVertex:
    __slots__ = ('n', 'index', 'lowlink', 'on_stack', 'successors', 'scc')
    def __init__(self, n):
        self.n = n
        self.index = -1
        self.successors = []
        
    def __repr__(self):
        return "TV[{}]".format(self.n)

### END TARJAN ###
s2i=lambda i:int(i)-1

N, M = map(int, input().strip().split())
G = Graph(N)
for _ in range(M):
    u,v=map(s2i, input().strip().split())
    G.add_edge(u,v,True,False)

print("SCC1:")
for scc in G.SCC1():
    print(scc)

print("SCC2:")
for scc in G.SCC2():
    print(scc)

