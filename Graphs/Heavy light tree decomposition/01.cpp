// https://www.hackerrank.com/rest/contests/world-codesprint-13/challenges/landslide/hackers/azneye/download_solution


/**
 * Heavy light decomposition of tree with N vertices.
 * Vertices are numbered 0 to N - 1.
 * Maps each vertex to an integer in [0, N-1] such that a subtree is a consecutive interval
 * and a path is at most O(log N) disjoint consecutive intervals.
 *
 * Can also be used for lca queries in O(log N) time per query
 *
 * O(N) space
 *
 * @tparam Alloc allocator to use
 *
 * @test http://www.spoj.com/problems/GRASSPLA/
 *
 * @test TODO: Non-commutative queries and updates like SPOJ GSS7
 */
template <template <class> class Alloc = allocator> class HLDOT {
  vector<vector<int, Alloc<int>>, Alloc<vector<int, Alloc<int>>>> adj;
  vector<int, Alloc<int>> lef, rig, size, par, top, depth;
  int next_num;
  vector<pair<int, int>> segs;

  void DfsSize(int at) {
    size[at] = 1;
    for (auto& to : adj[at]) {
      if (to == par[at])
        swap(to, adj[at].back());
      if (to != par[at]) {
        par[to] = at;
        depth[to] = depth[at] + 1;
        DfsSize(to);
        size[at] += size[to];
        if (size[to] > size[adj[at].front()])
          swap(to, adj[at].front());
      }
    }
    if (not adj[at].empty() and adj[at].back() == par[at])
      adj[at].erase(--adj[at].end());
  }

  void DfsHld(int at) {
    lef[at] = next_num++;
    for (const auto& to : adj[at]) {
      top[to] = to == adj[at].front() ? top[at] : to;
      DfsHld(to);
    }
    rig[at] = next_num - 1;
  }

  void QueryUp(int v, int lca) {
    while (top[v] != top[lca]) {
      segs.emplace_back(lef[top[v]], lef[v]);
      v = par[top[v]];
    }
    if (v != lca)
      segs.emplace_back(lef[lca] + 1, lef[v]);
  }

 public:
  HLDOT(int vertices) : adj(vertices), lef(vertices), rig(vertices), size(vertices), par(vertices),
                        top(vertices), depth(vertices) {}

  void AddEdge(int a, int b) {
    adj[a].push_back(b);
    adj[b].push_back(a);
  }

  /**
   * @return Is a ancestor of b?
   */
  bool IsAncestor(int a, int b) const {
    return lef[a] <= lef[b] && rig[b] <= rig[a];
  }

  int GetLca(int a, int b) const {
    while (top[a] != top[b]) {
      if (depth[top[a]] < depth[top[b]])
        swap(a, b);
      a = par[top[a]];
    }
    return depth[a] < depth[b] ? a : b;
  }

  int GetDepth(int v) const { return depth[v]; }

  int GetDist(int a, int b) const {
    return depth[a] + depth[b] - 2 * depth[GetLca(a, b)];
  }

  void Process(int root = 0) {
    next_num = 0;
    par[root] = top[root] = root;
    DfsSize(root);
    DfsHld(root);
  }

  pair<int, int> GetSubtreeSegment(int v) const {
    return {lef[v], rig[v]};
  }

  /**
   * @param include_lca: whether to include the lowest common ancestor in path or not
   * @return a list of segments of the given path. Segments are ordered from a to b so this works
   * for non-commutative path queries
   */
  const vector<pair<int, int>>& GetPathSegments(int a, int b, bool include_lca = true) {
    segs.clear();
    const int lca = GetLca(a, b);
    QueryUp(b, lca);
    if (include_lca)
      segs.emplace_back(lef[lca], lef[lca]);
    const auto count = segs.size();
    QueryUp(a, lca);
    reverse(segs.begin(), segs.end());
    reverse(segs.begin(), segs.end() - count);
    return segs;
  }
};