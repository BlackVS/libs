// https://www.hackerrank.com/rest/contests/world-codesprint-13/challenges/landslide/hackers/azneye/download_solution

/**
 * Zero-indexed Fenwick Tree with generic identity element and combine function.
 * Default is sum on ints and is well tested.
 * @tparam T
 * @tparam Func combining functor, (T,T) -> T
 * @tparam Alloc allocator to use
 */
template <class T = int, class Func = plus<T>, template <class> class Alloc =allocator>
class FenwickTree {
  vector<T, Alloc<T>> tree;
  const int size;
  const Func func;
  const T iden;

 public:
  FenwickTree(int size, const T& iden = 0) : tree(size + 1, iden), size(size), func(), iden(iden) {}

  void Update(int pos, const T& val) {
    for (int i = pos + 1; i <= size; i += i & -i)
      tree[i] = func(tree[i], val);
  }

  T Query(int pos) const {
    T ans(iden);
    for (int i = pos + 1; i > 0; i -= i & -i)
      ans = func(ans, tree[i]);
    return ans;
  }

  T Query(int lef, int rig) const {
    return Query(rig) - Query(lef - 1);
  }
};