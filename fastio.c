void fastscan(int &number)
{
	//variable to indicate sign of input number
	bool negative = false;
	register int c;

	number = 0;

	// extract current character from buffer
	do {
		c = getchar_unlocked();
	} while (c == ' ' || c == '\n');

	if (c == '-')
	{
		// number is negative
		negative = true;

		// extract the next character from the buffer
		c = getchar_unlocked();
	}

	// Keep on extracting characters if they are integers
	// i.e ASCII Value lies from '0'(48) to '9' (57)
	for (; (c>47 && c<58); c = getchar_unlocked())
		number = number * 10 + c - 48;

	// if scanned input has a negative sign, negate the
	// value of the input number
	if (negative)
		number *= -1;
}

template<typename T> void scan(T &x)
{
    x = 0;
    bool neg = 0;
    register T c = getchar();
 
    if (c == '-')
        neg = 1, c = getchar();
 
    while ((c < 48) || (c > 57))
        c = getchar();
 
    for ( ; c < 48||c > 57 ; c = getchar());
 
    for ( ; c > 47 && c < 58; c = getchar() )
        x= (x << 3) + ( x << 1 ) + ( c & 15 );
 
    if (neg) x *= -1;
}
 
template<typename T> void print(T n)
{
    bool neg = 0;
 
    if (n < 0)
        n *= -1, neg = 1;
 
    char snum[65];
    int i = 0;
    do
    {
        snum[i++] = n % 10 + '0';
        n /= 10;
    }
 
    while (n);
    --i;
 
    if (neg)
        putchar_unlocked('-');
 
    while (i >= 0)
        putchar_unlocked(snum[i--]);
 
    putchar_unlocked('\n');
}