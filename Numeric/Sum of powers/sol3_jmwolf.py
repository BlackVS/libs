#!/bin/python3

import sys

MOD = 1000000009

###fast choose function (need to go up to 1001)
Factorial = [1]*1002
Factorial_inverse = [1]*1002
for k in range(1, 1002):
    Factorial[k] = (Factorial[k-1]*k)%MOD
    Factorial_inverse[k] = pow(Factorial[k], MOD-2, MOD)
    
def choose(a, b):
    return (Factorial[a] * Factorial_inverse[b] * Factorial_inverse[a-b])%MOD



###precomputation of Bernoulli numbers for this computation
B = [1]*1001
for m in range(1, 1001):
    S = 0
    for k in range(m):
        S = (S + choose(m,k) * B[k] * pow(m-k+1, MOD-2, MOD))%MOD
    B[m] = (1 - S)%MOD

    
def highwayConstruction(n, k):
    sol = 0
    for j in range(k+1):
        sol = (sol + pow(-1,j)*choose(k+1, j) * B[j] * pow(n, k+1-j, MOD))%MOD
    sol = (sol*pow(k+1, MOD-2, MOD))%MOD
    
    return sol
    


q = int(input().strip())
for _ in range(q):
    n, k = input().strip().split(' ')
    n, k = [int(n), int(k)]
    
    factor = n//MOD
    rem = n % MOD
    if n == 1:
        print(0)
    else:
        result = (factor * highwayConstruction(MOD, k))%MOD
        result = (result + highwayConstruction(rem, k) - 1)%MOD