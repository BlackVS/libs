MODN=1000000000+7
PIS =2000000016
MAXFS=10
FMEM=dict()

def fib(self,n):
    if n<MAXFS:
        return FS[n]
    n+=1
    res=FMEM.get(n,None)
    if res:
        return res
    if n>=PIS: 
        n%=PIS
    v1, v2, v3 = 1, 1, 0    # initialise a matrix [[1,1],[1,0]]
    for rec in bin(n)[3:]:  # perform fast exponentiation of the matrix (quickly raise it to the nth power)
        calc = v2*v2
        if calc>=MODN: calc%=MODN
        v1, v2, v3 = v1*v1+calc, (v1+v3)*v2, calc+v3*v3
        if v1>=MODN: v1%=MODN
        if v2>=MODN: v2%=MODN
        if v3>=MODN: v3%=MODN
        if rec=='1':    
            v1, v2, v3 = v1+v2, v1, v2
            if v2>=MODN: v2%=MODN
    res=v2
    FMEM[n]=res
    return res
