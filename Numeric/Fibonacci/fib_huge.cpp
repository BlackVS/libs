// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#include <iostream>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <stdbool.h>
#include <set>
#include <map>
#include <utility>
#include <unordered_map>

#define MAXN 8192
#define FWTYPE short
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))


long long get_pisano_period(long long m) {
	long long a = 0, b = 1, c = a + b;
	for (int i = 0; i < m * m; i++) {
		c = (a + b) % m;
		a = b;
		b = c;
		if (a == 0 && b == 1) return i + 1;
	}
}

long long get_fibonacci_huge(long long n, long long m, long long p) {
	long long remainder = n % p;// get_pisano_period(m);

	long long first = 0;
	long long second = 1;

	long long res = remainder;

	for (int i = 1; i < remainder; i++) {
		res = (first + second) % m;
		first = second;
		second = res;
	}

	return res % m;
}

int main() {
	//std::cout << get_pisano_period(1000000007);
	long long p = 2000000016;
	long long m = 1000000000 + 7;
	long long n = 20000000000;
	std::cout << get_fibonacci_huge(20000000000, m, p);
	std::cout << get_fibonacci_huge(2000000000, m, p);
	std::cout << get_fibonacci_huge(200000000, m, p);
	std::cout << get_fibonacci_huge(20000000, m, p);
	return 0;
}

