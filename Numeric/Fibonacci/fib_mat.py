MODN=1000000000+7
PIS =2000000016
MAXFS=10

# [a b c d] => [ a b ]
#              [ c d ]
matE = [1, 0, 0, 1];
matF = [1, 1, 1, 0];

def modmod(X):
    for i in range(len(X)):
        if X[i]>=MODN: X[i]%=MODN
    return X

def matadd(A,B):
    return modmod(list(map(add,A,B)))
   

def matdiff(A,B):
    return modmod(list(map(sub,A,B)))

def matmul(A,B):
    C = [ A[0]*B[0]+A[1]*B[2], A[0]*B[1]+A[1]*B[3],
          A[2]*B[0]+A[3]*B[2], A[2]*B[1]+A[3]*B[3] ]
    return modmod(C)

def matmuls(c,A):
    return [ (c*a)%MODN for a in A]

def matprint(M):
    print("{0:3} {1:3}\n{2:3} {3:3}".format(*M))

@lru_cache(None)
def fib(n):
    if n == 0:
        return [1, 0, 0, 1]
    if n>=PIS: 
        n%=PIS
    Y = [1, 0, 0, 1];
    X = [1, 1, 1, 0];
    while n > 1:
        if (n&1)==0: 
            X = matmul(X,X);
        else:
            Y = matmul(X,Y);
            X = matmul(X,X);
        n //= 2;
    return matmul(X,Y)
