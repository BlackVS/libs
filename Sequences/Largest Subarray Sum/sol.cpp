// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>
#include <set>
#include <utility>
#include <cassert>
using namespace std;

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define boost std::ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0)

typedef vector< int > VI;
typedef vector< VI> ARRAY;


void PrintArray(ARRAY& arr) {
	for (int r = 0; r < arr.size(); r++) {
		for (int c = 0; c < arr[0].size(); c++)
		{
			cout << arr[r][c] << " ";
		}
		cout << endl;
	}
}

//vector< vector< int >> MEMMAX_C(MAXM, vector<int>(MAXM));
//vector< vector< int >> MEMMAX_R(MAXN, vector<int>(MAXN));

//vector<bool> CHANGED_C(MAXM, false);
//vector<bool> CHANGED_R(MAXM, false);


int kadane(VI& arr, int& start, int& finish)
{
	// initialize sum, maxSum and
	int sum = 0, maxSum = INT_MIN, i;

	// Just some initial value to check for all negative values case
	finish = -1;

	// local variable
	int local_start = 0;

	for (i = 0; i < arr.size(); ++i)
	{
		sum += arr[i];
		if (sum < 0)
		{
			sum = 0;
			local_start = i + 1;
		}
		else if (sum > maxSum)
		{
			maxSum = sum;
			start = local_start;
			finish = i;
		}
	}

	// There is at-least one non-negative number
	if (finish != -1)
		return maxSum;

	// Special Case: When all numbers in arr[] are negative
	maxSum = arr[0];
	start = finish = 0;

	// Find the maximum element in array
	for (i = 1; i < arr.size(); i++)
	{
		if (arr[i] > maxSum)
		{
			maxSum = arr[i];
			start = finish = i;
		}
	}
	return maxSum;
}

VI columnSum;
int findMaxSum(ARRAY& arr)
{
	//printArr();
	int s = INT_MIN, S = INT_MIN, t;
	int N = (int)arr.size();
	int M = (int)arr[0].size();
	for (int row = 0; row<N; row++)
	{
		for (int i = 0; i<M; i++)
			columnSum[i] = 0;
		for (int x = row; x<N; x++)
		{
			s = INT_MIN;
			t = 0;
			for (int i = 0; i<M; i++)
			{
				columnSum[i] += arr[x][i];
				t += columnSum[i];
				if (t>s)
					s = t;
				if (t<0)
					t = 0;
			}
			if (s>S)
				S = s;
		}
	}
	return S;
}

// The main function that finds maximum sum rectangle in M[][]
vector<bool> CHANGED_C;
ARRAY MEMMAX_C;
int findMaxSumC(ARRAY& arr, bool fUpdateOnly = false, bool fResetChanged = false)
{
	int maxSum = INT_MIN;

	int left, right, i;
	int sum, start, finish;

	int N = (int)arr.size();
	int M = (int)arr[0].size();

	VI temp(N);
	for (left = 0; left < M; ++left)
	{
		for (i = 0; i < N; ++i)
			temp[i] = arr[i][left];

		bool bProcess = !fUpdateOnly;
		if (fUpdateOnly)
			bProcess = CHANGED_C[left];

		for (right = left; right < M; ++right)
		{
			if (fUpdateOnly)
				bProcess = bProcess || CHANGED_C[right];

			if (right>left)
				for (i = 0; i < N; ++i)
					temp[i] += arr[i][right];

			if (bProcess) 
			{
				sum = kadane(temp, start, finish);
				MEMMAX_C[left][right] = sum;
			}
			else {
				sum = MEMMAX_C[left][right];
				//int s = kadane(temp, start, finish);
				//assert(s == sum);
			}
			if (sum > maxSum)
				maxSum = sum;
		}
		if (fResetChanged)
			CHANGED_C[left] = false;
	}
	return maxSum;
}

// The main function that finds maximum sum rectangle in M[][]
vector<bool> CHANGED_R;
ARRAY MEMMAX_R;
int findMaxSumR(ARRAY& arr, bool fUpdateOnly = false, bool fResetChanged = false)
{
	int maxSum = INT_MIN;

	int sum, start, finish;

	int N = (int)arr.size();
	int M = (int)arr[0].size();

	VI temp(M);
	for (int top = 0; top < N; top++)
	{
		for (int i = 0; i < M; ++i)
			temp[i] = arr[top][i];

		bool bProcess = !fUpdateOnly;
		if (fUpdateOnly)
			bProcess = CHANGED_R[top];

		for (int bottom = top; bottom < N; bottom++)
		{
			if (fUpdateOnly)
				bProcess = bProcess || CHANGED_R[bottom];

			if (bottom>top)
				for (int i = 0; i < M; ++i)
					temp[i] += arr[bottom][i];

			if (bProcess) 
			{
				sum = kadane(temp, start, finish);
				MEMMAX_R[top][bottom] = sum;
			}
			else {
				sum = MEMMAX_R[top][bottom];
				//int s = kadane(temp, start, finish);
				//assert(s == sum);
			}
			if (sum > maxSum)
				maxSum = sum;
		}
		//if (fResetChanged)
		//	CHANGED_R[top] = false;
	}
	return maxSum;
}


int randint(int range_min, int range_max)
{
	// [range_min, range_max]. In other words,
	return (rand()*(range_max - range_min + 1)) / RAND_MAX  + range_min;
}

int main() {
	boost;
	int N = 100;
	int M = 200;
	int MINV = -1000;
	int MAXV = 1000;

	ARRAY arr0(N, VI(M));
	columnSum = VI(M, 0);
	CHANGED_C = vector<bool>(M, false);
	CHANGED_R = vector<bool>(N, false);
	MEMMAX_C = ARRAY(M, VI(M));
	MEMMAX_R = ARRAY(N, VI(N));
	//
	srand((unsigned)time(NULL));
	for (int i = 0; i < N; i++)
		for (int j = 0; j < M; j++)
			arr0[i][j] = randint(MINV, MAXV);
	//PrintArray(arr0);

	int MAXL = 5;
	VI R(MAXL);
	VI C(MAXL);
	VI V(MAXL);
	for (int i = 0; i < MAXL; i++) {
		R[i] = randint(0, N-1);
		assert(R[i] >= 0 && R[i] < N);
		C[i] = randint(0, M-1);
		assert(C[i] >= 0 && C[i] < M);
		V[i] = randint(MINV, MAXV);
		assert(V[i] >= MINV && V[i] <= MAXV);
	}

	int res0 = findMaxSum (arr0);
	int res1 = findMaxSumC(arr0);
	int res2 = findMaxSumR(arr0);
	if (res1 != res2 || res0 != res2) {
		cout << "FAILED";
		return 0;
	}
	cout << "OK" << endl;
	//
	ARRAY arr = arr0;
	clock_t begin = clock();
	for (int i = 0; i < MAXL; i++) {
		arr[R[i]][C[i]] = V[i];
		res0 = findMaxSum (arr);
		res1 = findMaxSumC(arr);
		res2 = findMaxSumR(arr);
		if (res1 != res2 || res0 != res2) {
			cout << "FAILED";
			return 0;
		}
	}
	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << " F+C+R = " << elapsed_secs << " secs" << endl;


	//
	arr = arr0;
	begin = clock();
	for (int i = 0; i < MAXL; i++) {
		arr[R[i]][C[i]] = V[i];
		res1 = findMaxSum (arr);
	}
	end = clock();
	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << " F = " << elapsed_secs << " secs" << endl;

	//
	arr = arr0;
	begin = clock();
	for (int i = 0; i < MAXL; i++) {
		arr[R[i]][C[i]] = V[i];
		res1 = findMaxSumC(arr);
	}
	end = clock();
	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << " C = " << elapsed_secs << " secs" << endl;

	//
	arr = arr0;
	res1 = findMaxSumC(arr);
	begin = clock();
	for (int i = 0; i < MAXL; i++) {
		arr[R[i]][C[i]] = V[i];
		CHANGED_C[C[i]] = true;
		res1 = findMaxSumC(arr,true,true);
	}
	end = clock();
	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << " CU = " << elapsed_secs << " secs" << endl;

	//
	arr = arr0;
	begin = clock();
	for (int i = 0; i < MAXL; i++) {
		arr[R[i]][C[i]] = V[i];
		res1 = findMaxSumR(arr0);
	}
	end = clock();
	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << " R = " << elapsed_secs << " secs" << endl;

	//
	arr = arr0;
	res1 = findMaxSumR(arr);
	begin = clock();
	for (int i = 0; i < MAXL; i++) {
		arr[R[i]][C[i]] = V[i];
		CHANGED_R[R[i]] = true;
		res1 = findMaxSumR(arr, true, true);
	}
	end = clock();
	elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout << " RU = " << elapsed_secs << " secs" << endl;

	return 0;
}