// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;

 

int longest = 0;
int a[1000005];
const int inf = 2000000000;

void find(int x) {
	//a[i] the last element of non-decreasing subsequence of length i. Note that a[i] is increasing. So we can binary search in it to find a position of the new element x. O(logn)
	int left = 1, right = longest;
	while (left <= right) {

		int mid = (left + right) >> 1;
		if (a[mid] <= x) {
			left = mid + 1;
		}
		else {
			right = mid - 1;
		}
	}
	a[++right] = x;
	if (right > longest) {
		longest = right;
	}
}


// Binary search (note boundaries in the caller)
int CeilIndexRight(std::vector<int> &v, int l, int r, int key) {
    while (r-l > 1) {
    int m = l + (r-l)/2;
    if (v[m] > key)
        r = m;
    else
        l = m;
    }
 
    return r;
}

int CeilIndexLeft(std::vector<int> &v, int l, int r, int key) {
	while (r - l > 1) {
		int m = l + (r - l) / 2;
		if (v[m] >= key)
			r = m;
		else
			l = m;
	}

	return r;
}


//! increasing!!!!
int LIS(std::vector<int> &v) {
    if (v.size() == 0)
        return 0;
 
    //std::vector<int> tail(v.size(), 0);

	// fill inf due allow work upper_bound - or it will point end()!
	std::vector<int> tail(v.size(), inf);

	int length = 1; // always points empty slot in tail
 
    tail[0] = v[0];
	auto pmax = next(tail.begin());
	for (size_t i = 1; i < v.size(); i++) {
        if (v[i] < tail[0])
            // new smallest value
            tail[0] = v[i];
		else if (v[i] > tail[length - 1]) {
			// v[i] extends largest subsequence
			tail[length++] = v[i];
			pmax = next(pmax);
		} else {
			// v[i] will become end candidate of an existing subsequence or
			// Throw away larger elements in all LIS, to make room for upcoming grater elements than v[i]
			// (and also, v[i] would have already appeared in one of LIS, identify the location and replace it)
			//tail[CeilIndexLeft(tail, -1, length-1, v[i])] = v[i];

			// to use upper_bound - fill array by inf first! -> arrayt MUST BE ordered any time
			auto p = std::lower_bound(tail.begin(), pmax, v[i]);
			*p = v[i];
		}
	}
 
    return length;
}

//longest strongly increasing
//https://www.geeksforgeeks.org/longest-increasing-subsequence/
int lis(std::vector<int>& arr, int max_ref=1)
{
	int n = (int)arr.size();
	/* Base case */
	if (n == 1)
		return 1;

	// 'max_ending_here' is length of LIS ending with arr[n-1]
	int res, max_ending_here = 1;

	/* Recursively get all LIS ending with arr[0], arr[1] ...
	arr[n-2]. If   arr[i-1] is smaller than arr[n-1], and
	max ending with arr[n-1] needs to be updated, then
	update it */
	for (int i = 1; i < n; i++)
	{
		res = lis(arr, max_ref);
		if (arr[i - 1] < arr[n - 1] && res + 1 > max_ending_here)
			max_ending_here = res + 1;
	}

	// Compare max_ending_here with the overall max. And
	// update the overall max if needed
	if (max_ref < max_ending_here)
		max_ref = max_ending_here;

	// Return length of LIS ending with arr[n-1]
	return max_ending_here;
}



//LIS !!!!
int main() {
    int N,d;
    std::cin >> N;
	std::vector<int> v;

    for(int i = 1; i <= N; i++) {
        cin >> d;
        v.push_back(d);
	}
	//int res = lis(v);
	int res = LIS(v);

	cout << res << endl;
    return 0;
}    
