#!/bin/python3
import sys

fstd="t1"
sys.stdin = open('..\\..\\%s.txt' % fstd, 'r')
#sys.stdout = open('..\\..\\%s.out' % fstd , 'w+')
#input = sys.stdin.readline

#sys.setrecursionlimit(10000)

class Node:
    def __init__(self,v,n):
        self.data=v
        self.serial=n
    def __lt__(self, other): #Less than
        return self.data<other.data
    def __le__(self, other): #Less than or equal to
        return self.data<=other.data
    def __eq__(self, other): #Equal to
        return self.data==other.data
    def __ne__(self, other): #Not equal to
        return self.data!=other.data
    def __gt__(self, other): #Greater than
        return self.data>other.data
    def __ge__(self, other): #Greater than or equal to
        return self.data>=other.data
    def __str__(self):
        return "data={0} (serial={1})".format(self.data,self.serial)

class RQMaxSum:
    def __init__(self,n):
        N
        self.serial=n

from timeit import default_timer as timer
def timefunc(func, *args, **kwargs):
    """Time a function. 
    args:
        iterations=1
    Usage example:
        timeit(myfunc, 1, b=2)
    """
    try:
        iterations = kwargs.pop('iterations')
    except KeyError:
        iterations = 1
    elapsed = sys.maxsize
    start = timer()
    for _ in range(iterations):
        result = func(*args, **kwargs)
    elapsed = (timer() - start)/iterations

    print(('{}() : {:.9f}'.format(func.__name__, elapsed)))
    return result
#res=timefunc( searchKMP, S, P, iterations=rep)
#if(res!=res0): print("Wrong")


if __name__ == "__main__":
    n1=Node(10,15)
    n2=Node(12,20)
    print(min(n1,n2))
