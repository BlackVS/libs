#!/bin/python3
import sys,copy
from collections import *
from itertools import *
from random import *


from timeit import default_timer as timer
def timefunc(func, *args, **kwargs):
    """Time a function. 
    args:
        iterations=1
    Usage example:
        timeit(myfunc, 1, b=2)
    """
    try:
        iterations = kwargs.pop('iterations')
    except KeyError:
        iterations = 1
    elapsed = sys.maxsize
    start = timer()
    for _ in range(iterations):
        result = func(*args, **kwargs)
    elapsed = (timer() - start)/iterations

    print(('{}() : {:.9f}'.format(func.__name__, elapsed)))
    return result
#res=timefunc( searchKMP, S, P, iterations=rep)


class SegmentTreeNode(object):
    def __init__(self, nodeRange, default=None):
        self.nodeRange = nodeRange
        self.value = default
        self.children = []
        self.parent = None

class SegmentTree(object):
    @staticmethod
    def __covers(a, b):
        return a[0] <= b[0] and b[1] <= a[1]

    @staticmethod
    def __intersects(a, b):
        return not (a[1] < b[0] or a[0] > b[1])

    def __init__(self, N, default=None, func=min, maxChildNum=2):
        self.func = func

        l = [SegmentTreeNode((k, k), default) for k in range(N)]
        self.mapping = {n.nodeRange[0]: n for n in l}

        while len(l) > 1:
            nl = []

            for i in range(0, len(l), maxChildNum):
                c = l[i:i+maxChildNum]
                n = SegmentTreeNode((c[0].nodeRange[0], c[-1].nodeRange[1]), default)
                n.children = c
                for x in c:
                    x.parent = n

                nl.append(n)

            l = nl

        self.root = l[0]

    def update(self, keyValueDict):
        ul = set()

        for (k, v) in keyValueDict.items():
            node = self.mapping[k]

            if node.value != v:
                node.value = v
                if node.parent != None:
                    ul.add(node.parent)

        while len(ul) > 0:
            newUL = set()

            for node in ul:
                pv = self.func(x.value for x in node.children)
                if pv != node.value:
                    node.value = pv
                    if node.parent != None:
                        newUL.add(node.parent)

            ul = newUL

    def init(self, X):
        ul = set()

        for (k, v) in enumerate(X):
            node = self.mapping[k]

            if node.value != v:
                node.value = v
                if node.parent != None:
                    ul.add(node.parent)

        while len(ul) > 0:
            newUL = set()

            for node in ul:
                pv = self.func(x.value for x in node.children)
                if pv != node.value:
                    node.value = pv
                    if node.parent != None:
                        newUL.add(node.parent)

            ul = newUL
    def update(self, k, v):
        ul = set()

        node = self.mapping[k]

        if node.value != v:
            node.value = v
            if node.parent != None:
                ul.add(node.parent)

        while len(ul) > 0:
            newUL = set()

            for node in ul:
                pv = self.func(x.value for x in node.children)
                if pv != node.value:
                    node.value = pv
                    if node.parent != None:
                        newUL.add(node.parent)

            ul = newUL

    def query(self, queryRange):
        def query_aux(node):
            if SegmentTree.__covers(queryRange, node.nodeRange):
                return node.value

            return self.func(
                query_aux(cNode)
                for cNode in node.children
                if SegmentTree.__intersects(queryRange, cNode.nodeRange)
            )

        if queryRange[0] > queryRange[1]:
            raise

        if not SegmentTree.__intersects(queryRange, self.root.nodeRange):
            raise

        return query_aux(self.root)

def test1(N,Q,MINV,MAXV,fn):
    X=[randint(MINV,MAXV) for _ in range(N)]
    for _ in range(Q):
        l,r=randint(0,N-1),randint(0,N-1)
        l,r=min(l,r), max(l,r)
        #updates
        idx=randint(0,N-1)
        v  =randint(MINV,MAXV)
        X[idx]=v
        ###
        res1=fn(X[l:r+1])

def test2(N,Q,MINV,MAXV,fn):
    X=[randint(MINV,MAXV) for _ in range(N)]
    rmq=SegmentTree(N,0,fn)
    rmq.init(X)
    for _ in range(Q):
        l,r=randint(0,N-1),randint(0,N-1)
        l,r=min(l,r), max(l,r)
        #updates
        idx=randint(0,N-1)
        v  =randint(MINV,MAXV)
        rmq.update(idx,v)
        ###
        res2=rmq.query( (l,r) )

def test():
    N=1000
    MINV,MAXV=-100,100
    Q=1000
    #fn=max
    fn=sum

    X=[randint(MINV,MAXV) for _ in range(N)]
    rmq=SegmentTree(N,0,fn)
    rmq.init(X)

    for _ in range(Q):
        l,r=randint(0,N-1),randint(0,N-1)
        l,r=min(l,r), max(l,r)
        #updates
        idx=randint(0,N-1)
        v  =randint(MINV,MAXV)
        X[idx]=v
        rmq.update(idx,v)
        ###
        res1=fn(X[l:r+1])
        res2=rmq.query( (l,r) )
        assert(res1==res2)
    print("OK")

    timefunc(test1, N,Q*10,MINV,MAXV,fn, iterations=1)
    timefunc(test2, N,Q*10,MINV,MAXV,fn, iterations=1)

test()