#!/bin/python3
import sys,copy
from collections import *
from itertools import *
from random import *

def minNone(a,b):
    if a==None and b==None:
        return None
    if a==None:
        return b
    if b==None:
        return a
    return min(a,b)

def maxNone(a,b):
    if a==None and b==None:
        return None
    if a==None:
        return b
    if b==None:
        return a
    return max(a,b)

class RMQ:
    def __init__(self, n):
        self.sz = 1 
        self.n  = n
        while self.sz <= n: 
            self.sz = self.sz << 1
        self.mins = [None] * (2*self.sz-1)
        self.maxs = [None] * (2*self.sz-1)
    
    #if N<<sz - skip Nones
    def init(self,X):
        for idx,x in enumerate(X):
            i=idx+self.sz - 1
            self.mins[i] = x
            self.maxs[i] = x
        for idx in range(self.sz-2, -1, -1):
            self.mins[idx] = minNone(self.mins[idx * 2 + 1], self.mins[idx * 2 + 2])
            self.maxs[idx] = maxNone(self.maxs[idx * 2 + 1], self.maxs[idx * 2 + 2])

    #updates up to root
    def updateMM(self, idx, x):
        idx += self.sz - 1
        self.mins[idx] = x
        self.maxs[idx] = x
        while idx > 0:
            idx = (idx - 1) >> 1
            self.mins[idx] = minNone(self.mins[idx * 2 + 1], self.mins[idx * 2 + 2])
            self.maxs[idx] = maxNone(self.maxs[idx * 2 + 1], self.maxs[idx * 2 + 2])


    #updates only if needed
    def updateMin(self, idx, x):
        idx += self.sz - 1
        self.mins[idx] = x
        while idx > 0:
            idx = (idx - 1) >> 1
            m = minNone(self.mins[idx * 2 + 1], self.mins[idx * 2 + 2])
            if m==self.mins[idx]:
                break
            self.mins[idx] = m

    #updates only if needed
    def updateMax(self, idx, x):
        idx += self.sz - 1
        self.maxs[idx] = x
        while idx > 0:
            idx = (idx - 1) >> 1
            m=maxNone(self.maxs[idx * 2 + 1], self.maxs[idx * 2 + 2])
            if m==self.maxs[idx]:
                break
            self.maxs[idx] = m
            
    def _query_minmax(self, a, b, k, l, r):
        if r < a or b < l:
            return (None,None)
        elif a <= l and r <= b:
            return (self.mins[k],self.maxs[k])
        else:
            m1=self._query_minmax(a, b, 2*k+1, l,          (l+r)>>1 )
            m2=self._query_minmax(a, b, 2*k+2, ((l+r)>>1)+1, r      )
            return (minNone(m1[0],m2[0]),maxNone(m1[1],m2[1]))

    def query_minmax(self, a, b):
        return self._query_minmax(a, b, 0, 0, self.sz-1)



def test():
    N=1000
    MINV,MAXV=-100,100
    Q=1000

    #X=[1,5,6,7,-5,-3,10,-1,60,99] #  [randint(MINV,MAXV) for _ in range(N)]
    X= [randint(MINV,MAXV) for _ in range(N)]
    
    rmq=RMQ(N)
    rmq.init(X)

    #i=5
    #v=-100
    #X[i]=v
    #rmq.update(i,v)
    #l,r=3,8
    #res1 = (min(X[l:r+1]),max(X[l:r+1]))
    #res2 = rmq.query_minmax( l,r )
    #assert(res1==res2)

    for _ in range(Q):
        l,r=randint(0,N-1),randint(0,N-1)
        l,r=min(l,r), max(l,r)
        #updates
        idx=randint(0,N-1)
        v  =randint(MINV,MAXV)
        X[idx]=v
        #rmq.updateMM(idx,v)
        rmq.updateMin(idx,v)
        rmq.updateMax(idx,v)
        ###
        res1 = (min(X[l:r+1]),max(X[l:r+1]))
        res2 = rmq.query_minmax( l,r )
        assert(res1==res2)
    print("OK")


test()