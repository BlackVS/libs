#!/bin/python3
import sys,copy
from collections import *
from itertools import *
from random import *

class RMQ(object):
    def __init__(self, a, fn):
        self._a=a
        self._n=n=len(a)
        self._logtable=_logtable=[0]*(n+1)
        self._fn=fn
        for i in range(2,n+1):
            _logtable[i]=_logtable[i>>1]+1
        
        self._rmq = rmq = [ [0]*n for _ in range(_logtable[n] + 1)]

        for i in range(n):
            rmq[0][i] = i

        k=1
        while (1 << k) < n:
            i=0
            while i + (1 << k) <= n:
                x = rmq[k - 1][i];
                y = rmq[k - 1][i + (1 << k - 1)];
                rmq[k][i] = x if self._fn(a[x],a[y])==a[x] else y
                i+=1
            k+=1

    def query(self, i, j):
        k = self._logtable[j - i]
        x = self._rmq[k][i]
        y = self._rmq[k][j - (1 << k) + 1]
        return x if self._fn(self._a[x],self._a[y])==self._a[x] else y

def test():
    N=1000
    MINV,MAXV=-100,100
    Q=1000
    fn=max

    X=[randint(MINV,MAXV) for _ in range(N)]
    rmq=RMQ(X,fn)
    for _ in range(Q):
        l,r=randint(0,N-1),randint(0,N-1)
        l,r=min(l,r), max(l,r)
        res1=fn(X[l:r+1])
        res2=X[rmq.query(l,r)]
        assert(res1==res2)
    print("OK")


test()