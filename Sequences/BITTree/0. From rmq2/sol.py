#!/bin/python3
import sys,copy
from collections import *
from itertools import *
from random import *


from timeit import default_timer as timer
def timefunc(func, *args, **kwargs):
    """Time a function. 
    args:
        iterations=1
    Usage example:
        timeit(myfunc, 1, b=2)
    """
    try:
        iterations = kwargs.pop('iterations')
    except KeyError:
        iterations = 1
    elapsed = sys.maxsize
    start = timer()
    for _ in range(iterations):
        result = func(*args, **kwargs)
    elapsed = (timer() - start)/iterations

    print(('{}() : {:.9f}'.format(func.__name__, elapsed)))
    return result
#res=timefunc( searchKMP, S, P, iterations=rep)





# Python implementation of Binary Indexed Tree
 
# Returns sum of arr[0..index]. This function assumes
# that the array is preprocessed and partial sums of
# array elements are stored in BITree[].
def getsum(BITTree,i):
    s = 0  #initialize result
    # index in BITree[] is 1 more than the index in arr[]
    i = i+1
    # Traverse ancestors of BITree[index]
    while i > 0:
        # Add current element of BITree to sum
        s += BITTree[i]
        # Move index to parent node in getSum View
        i -= i & (-i)
    return s
 
# Updates a node in Binary Index Tree (BITree) at given index
# in BITree.  The given value 'val' is added to BITree[i] and
# all of its ancestors in tree.
def updatebit(BITTree , n , i ,v):
    # index in BITree[] is 1 more than the index in arr[]
    i += 1
    # Traverse all ancestors and add 'val'
    while i <= n:
        # Add 'val' to current node of BI Tree
        BITTree[i] += v
        # Update index to that of parent in update View
        i += i & (-i)
 
# Constructs and returns a Binary Indexed Tree for given
# array of size n.
def construct(arr, n):
    # Create and initialize BITree[] as 0
    BITTree = [0]*(n+1)
    # Store the actual values in BITree[] using update()
    for i in range(n):
        updatebit(BITTree, n, i, arr[i])
    # Uncomment below lines to see contents of BITree[]
    #for i in range(1,n+1):
    #      print BITTree[i],
    return BITTree
 
# Driver code to test above methods
#freq = [2, 1, 1, 3, 2, 3, 4, 5, 6, 7, 8, 9]
#BITTree = construct(freq,len(freq))
#print("Sum of elements in arr[0..5] is " + str(getsum(BITTree,5)))
#freq[3] += 6
#updatebit(BITTree, len(freq), 3, 6)
#print("Sum of elements in arr[0..5] is " + str(getsum(BITTree,5)))
# This code is contributed by Raju Varshney

def test3(N,Q,MINV,MAXV,fn):
    X=[randint(MINV,MAXV) for _ in range(N)]
    BITTree = construct(X,len(X))
    for _ in range(Q):
        l,r=randint(0,N-1),randint(0,N-1)
        l,r=min(l,r), max(l,r)
        #updates
        idx=randint(0,N-1)
        v  =randint(MINV,MAXV)
        X[idx]+=v
        rmq.update(idx,v)
        ###
        res2=rmq.query( (l,r) )





class SegmentTreeNode(object):
    def __init__(self, nodeRange, default=None):
        self.nodeRange = nodeRange
        self.value = default
        self.children = []
        self.parent = None

class SegmentTree(object):
    @staticmethod
    def __covers(a, b):
        return a[0] <= b[0] and b[1] <= a[1]

    @staticmethod
    def __intersects(a, b):
        return not (a[1] < b[0] or a[0] > b[1])

    def __init__(self, N, default=None, func=min, maxChildNum=2):
        self.func = func

        l = [SegmentTreeNode((k, k), default) for k in range(N)]
        self.mapping = {n.nodeRange[0]: n for n in l}

        while len(l) > 1:
            nl = []

            for i in range(0, len(l), maxChildNum):
                c = l[i:i+maxChildNum]
                n = SegmentTreeNode((c[0].nodeRange[0], c[-1].nodeRange[1]), default)
                n.children = c
                for x in c:
                    x.parent = n

                nl.append(n)

            l = nl

        self.root = l[0]

    def update(self, keyValueDict):
        ul = set()

        for (k, v) in keyValueDict.items():
            node = self.mapping[k]

            if node.value != v:
                node.value = v
                if node.parent != None:
                    ul.add(node.parent)

        while len(ul) > 0:
            newUL = set()

            for node in ul:
                pv = self.func(x.value for x in node.children)
                if pv != node.value:
                    node.value = pv
                    if node.parent != None:
                        newUL.add(node.parent)

            ul = newUL

    def init(self, X):
        ul = set()

        for (k, v) in enumerate(X):
            node = self.mapping[k]

            if node.value != v:
                node.value = v
                if node.parent != None:
                    ul.add(node.parent)

        while len(ul) > 0:
            newUL = set()

            for node in ul:
                pv = self.func(x.value for x in node.children)
                if pv != node.value:
                    node.value = pv
                    if node.parent != None:
                        newUL.add(node.parent)

            ul = newUL
    def update(self, k, v):
        ul = set()

        node = self.mapping[k]

        if node.value != v:
            node.value = v
            if node.parent != None:
                ul.add(node.parent)

        while len(ul) > 0:
            newUL = set()

            for node in ul:
                pv = self.func(x.value for x in node.children)
                if pv != node.value:
                    node.value = pv
                    if node.parent != None:
                        newUL.add(node.parent)

            ul = newUL

    def query(self, queryRange):
        def query_aux(node):
            if SegmentTree.__covers(queryRange, node.nodeRange):
                return node.value

            return self.func(
                query_aux(cNode)
                for cNode in node.children
                if SegmentTree.__intersects(queryRange, cNode.nodeRange)
            )

        if queryRange[0] > queryRange[1]:
            raise

        if not SegmentTree.__intersects(queryRange, self.root.nodeRange):
            raise

        return query_aux(self.root)

def test1(N,Q,MINV,MAXV,fn):
    X=[randint(MINV,MAXV) for _ in range(N)]
    for _ in range(Q):
        l,r=randint(0,N-1),randint(0,N-1)
        l,r=min(l,r), max(l,r)
        #updates
        idx=randint(0,N-1)
        v  =randint(MINV,MAXV)
        X[idx]=v
        ###
        res1=fn(X[l:r+1])

def test2(N,Q,MINV,MAXV,fn):
    X=[randint(MINV,MAXV) for _ in range(N)]
    rmq=SegmentTree(N,0,fn)
    rmq.init(X)
    for _ in range(Q):
        l,r=randint(0,N-1),randint(0,N-1)
        l,r=min(l,r), max(l,r)
        #updates
        idx=randint(0,N-1)
        v  =randint(MINV,MAXV)
        rmq.update(idx,v)
        ###
        res2=rmq.query( (l,r) )

def test():
    N=1000
    MINV,MAXV=-100,100
    Q=1000
    #fn=max
    fn=sum
    X=[randint(MINV,MAXV) for _ in range(N)]
    #X=[1,2,3,4,5,6,7,8,9,0]
    #N=len(X)
    #Q=1
    #MINV,MAXV=-10,10





    rmq=SegmentTree(N,0,fn)
    rmq.init(X)

    BITTree = construct(X,len(X))

#print("Sum of elements in arr[0..5] is " + str(getsum(BITTree,5)))
#freq[3] += 6
#updatebit(BITTree, len(freq), 3, 6)
#print("Sum of elements in arr[0..5] is " + str(getsum(BITTree,5)))
# This code is contributed by Raju Varshney

    for _ in range(Q):
        l,r=randint(0,N-1),randint(0,N-1)
        l,r=min(l,r), max(l,r)
        #updates
        idx=randint(0,N-1)
        v  =randint(MINV,MAXV)
        oldv=X[idx] #needed also for BIT
        ###
        X[idx]=v #needed also for BIT ?
        res1=fn(X[l:r+1])
        ###
        rmq.update(idx,v)
        res2=rmq.query( (l,r) )
        ###
        updatebit(BITTree, len(X), idx, 6-oldv)
        sr=getsum(BITTree,r)
        sl=getsum(BITTree,l-1)
        assert(sr==fn(X[:r+1]))
        assert(sl==fn(X[:l]))
        res3=sr-sl
        # check
        assert(res1==res2)
        assert(res1==res3)
    print("OK")

    timefunc(test1, N,Q,MINV,MAXV,fn, iterations=1)
    timefunc(test2, N,Q,MINV,MAXV,fn, iterations=1)

test()