#!/bin/python3
import sys
from random import *
from collections import *
import timeit, time

#https://www.geeksforgeeks.org/next-greater-element/

def NGE_N2(arr):
    res=[(None,None)]*len(arr)
    for i in range(len(arr)):
        next = (None,None)
        for j in range(i+1, len(arr)):
            if arr[i] < arr[j]:
                next = (j, arr[j])
                break
        res[i]=next
    return res


# Python program to print next greater element using stack
# Stack Functions to be used by printNGE()
def NGE_Nv1(arr):
    res=[(None,None)]*len(arr)
    #
    stack = deque() 
    element = 0
    next = 0
 
    # push the first element to stack
    stack.append( (0,arr[0]) )
 
    # iterate for rest of the elements
    for i in range(1,len(arr)):
        next = arr[i]
 
        #stack[-1]<next or via pop/push if O(stack[-1])>Q(1)
        while stack and stack[-1][1]<next:
            j,element = stack.pop()
            res[j]=(i,next)

        stack.append( (i,next) )
 
    return res

#no difference with v1 perfomance
def NGE_Nv2(arr):
    res=[(None,None)]*len(arr)
    #
    stack = deque() 
    element = 0
    next = 0
 
    # push the first element to stack
    stack.append( (0,arr[0]) )
 
    # iterate for rest of the elements
    for i in range(1,len(arr)):
        next = arr[i]
 
        #stack[-1]<next or via pop/push if O(stack[-1])>Q(1)
        while stack:
            j,element = stack.pop()
            if element<next:
                res[j]=(i,next)
                continue
            stack.push( (j,element) )
            break

        stack.append( (i,next) )
    return res

def test(arr):
    #print(arr)
    
    start_time = timeit.default_timer()
    res1=NGE_N2(arr)
    elapsed = timeit.default_timer() - start_time
    print("Time N2=",elapsed)
    
    #print(res1)
    start_time = timeit.default_timer()
    res2=NGE_Nv1(arr)
    elapsed = timeit.default_timer() - start_time
    print("Time N =",elapsed)

    start_time = timeit.default_timer()
    res3=NGE_Nv1(arr)
    elapsed = timeit.default_timer() - start_time
    print("Time N =",elapsed)

    #print(res2)
    print(res1==res2)
    print(res1==res3)

def solve():
    N=10000
    arr=[randint(-100,100) for _ in range(N)]
    test(arr)
             

solve()