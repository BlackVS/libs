#!/bin/python3
import sys, string
from random import *
from timeit import default_timer as timer


def randstr(N,alphabet=string.ascii_lowercase):
    l=len(alphabet)
    return "".join( alphabet[randint(0,l-1)] for _ in range(N))


def timefunc(func, *args, **kwargs):
    """Time a function. 
    args:
        iterations=1
    Usage example:
        timeit(myfunc, 1, b=2)
    """
    try:
        iterations = kwargs.pop('iterations')
    except KeyError:
        iterations = 1
    elapsed = sys.maxsize
    start = timer()
    for _ in range(iterations):
        result = func(*args, **kwargs)
    elapsed = (timer() - start)/iterations

    print(('{}() : {:.9f}'.format(func.__name__, elapsed)))
    return result


#res=timefunc( searchKMP, S, P, iterations=rep)
#if(res!=res0): print("Wrong")

#http://e-maxx.ru/algo/prefix_function
#vector<int> prefix_function (string s) {
#	int n = (int) s.length();
#	vector<int> pi (n);
#	for (int i=0; i<n; ++i)
#		for (int k=0; k<=i; ++k)
#			if (s.substr(0,k) == s.substr(i-k+1,k))
#				pi[i] = k;
#	return pi;
#}
def getLPS_naive(S):
    N=len(S)
    P=[0]*N
    for i in range(N):
        for k in range(i+1):
            if S[:k]==S[i-k+1:i+i]:
                P[i]=k
    return P

# from RR 4, DNA
def getLPS_1(S):
    N=len(S)
    P=[0]*N
    k=-1
    P[0] = -1
    for i in range(1,N):
        while k > -1 and S[i] != S[k + 1]:
           k = P[k]
        if S[i] == S[k + 1]:
           k+=1
        P[i] = k
    return P

#https://www.geeksforgeeks.org/searching-for-patterns-set-2-kmp-algorithm/
def getLPS_2(pat):
    M=len(pat)
    lps=[0]*M
    #
    lenp = 0 # lenpgth of the previous longest prefix suffix
    lps[0] # lps[0] is always 0
    i = 1
    # the loop calculates lps[i] for i = 1 to M-1
    while i < M:
        if pat[i]==pat[lenp]:
            lenp += 1
            lps[i] = lenp
            i += 1
        else:
            # This is tricky. Consider the example.
            # AAACAAAA and i = 7. The idea is similar 
            # to search step.
            if lenp != 0:
                lenp = lps[lenp-1]
                # Also, note that we do not increment i here
            else:
                lps[i] = 0
                i += 1
    return lps

#http://e-maxx.ru/algo/prefix_function
#vector<int> prefix_function (string s) {
#	int n = (int) s.length();
#	vector<int> pi (n);
#	for (int i=1; i<n; ++i) {
#		int j = pi[i-1];
#		while (j > 0 && s[i] != s[j])
#			j = pi[j-1];
#		if (s[i] == s[j])  ++j;
#		pi[i] = j;
#	}
#	return pi;
#}
def getLPS_3(S):
    N=len(S)
    P=[0]*N
    for i in range(1,N):
        j=P[i-1]
        while j > 0 and S[i] != S[j]:
           j = P[j-1]
        if S[i] == S[j]:
           j+=1
        P[i] = j
    return P


if __name__ == "__main__":
    alpha="AB"
    #alpha=string.ascii_lowercase
    S = randstr(100000,alphabet=alpha)
    #S=['A']*10000
    #S="ababbbaababbba"
    rep=1

    #print(S)

    #res0=timefunc( getLPS_naive, S, iterations=rep)
    #print(res0)

    res0=timefunc( getLPS_1, S, iterations=rep)
    for i in range(len(S)): res0[i]+=1
    #print(res0==res)
    #print(res0)

    res=timefunc( getLPS_2, S, iterations=rep)
    #print(res)
    print(res0==res)

    res=timefunc( getLPS_3, S, iterations=rep)
    #print(res)
    print(res0==res)
