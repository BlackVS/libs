# Enter your code here. Read input from STDIN. Print output to STDOUT
from bisect import bisect_left
def LongestIncreasingSubsequence(S):
    
    head = []
    tail = [None]

    for x in S:
        i = bisect_left(head,x)
        if i >= len(head):
            head.append(x)
            if i > 0:
                tail.append((head[i-1],tail[i-1]))
        elif head[i] > x:
            head[i] = x
            if i > 0:
                tail[i] = head[i-1],tail[i-1]

    if not head:
        return []

    output = [head[-1]]
    pair = tail[-1]
    while pair:
        x,pair = pair
        output.append(x)

    output.reverse()
    return len(output)
n = int(raw_input())
a = list()
for i in range(n):
    temp = int(raw_input())
    a.append(temp)
print LongestIncreasingSubsequence(a)