// sol.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <set>
#include <map>
#include <iomanip>
using namespace std;


//vector of int
typedef vector<int> VI;

//vector of vector of int
typedef vector<VI>  VVI;

//struct for 1 solution
typedef struct {
	string s;
	VI  x;
	VI  y;
} SOLREC;

typedef vector<SOLREC> VSOL;

typedef pair<int,int> NODE;

typedef vector<NODE> NODES;

typedef map<int, NODES> MAPNODES;


// fill lookup table
void LCSinit(string X, string Y, VVI& lookup, MAPNODES& mnodes)
{
	size_t M = X.length();
	size_t N = Y.length();

	// first row and first column of the lookup table are already 0 
	// fill the lookup table in bottom-up manner
	for (int i = 1; i <= M; i++)
	{
		for (int j = 1; j <= N; j++)
		{
			// if current character of X and Y matches
			if (X[i - 1] == Y[j - 1]) {
				lookup[i][j] = lookup[i - 1][j - 1] + 1;
				mnodes[lookup[i][j]].push_back(NODE(i-1,j-1));
			}
			// else if current character of X and Y don't match
			else
				lookup[i][j] = max(lookup[i - 1][j], lookup[i][j - 1]);
		}
	}
}

void LCSprint(string X, string Y, VVI& lookup) 
{
	size_t M = X.length();
	size_t N = Y.length();
	const int W = 4;
	cout << setw(W) << "" << setw(W) << "*";
	for (auto y : Y)
		cout << setw(W) << y;
	cout << endl;
	for (int r = 0; r <= M; r++) {
		if (r == 0)
			cout << setw(W) << "*";
		else
			cout << setw(W) << X[r-1];
		for (int c = 0; c <= N; c++) {
			if(r>0 && c>0 && X[r-1]==Y[c-1])

				cout <<  setw(W) << -lookup[r][c];
			else
				cout << setw(W) << lookup[r][c];
		}
		cout << endl;
	}
}

void LCSupdateRes(VSOL& res, char c, int x, int y)
{
	if (res.size() == 0) {
		SOLREC r;
		r.s += c;
		r.x.push_back(x);
		r.y.push_back(y);
		res.push_back(r);
	}
	else {
		for (auto& r : res) {
			r.s += c;
			r.x.push_back(x);
			r.y.push_back(y);
		}
	}
}

VSOL LCSgetAll(string X, string Y, MAPNODES& mnodes, int x, int y, int cnt)
{
	VSOL res;
	for (auto node : mnodes[cnt - 1]) 
	{
		int nx = node.first;
		int ny = node.second;
		if (nx < x && ny < y) 
		{
			//possible extension - get all possible solutions for cnt-1 at this node and add to res
			VSOL r = LCSgetAll(X, Y, mnodes, nx, ny, cnt - 1);
			res.insert(res.end(), r.begin(), r.end());
		}
	}
	//add current symbol
	if (x < X.length()) {
		char c = X[x];
		LCSupdateRes(res, c, x, y);
	}
	return res;
}

// main function
int main()
{
	string X;
	string Y;
	cin >> X >> Y;
	size_t M = X.length();
	size_t N = Y.length();
	cout << "X: " << X << endl;
	cout << "Y: " << Y << endl;

	VVI lookup( M+1, VI(N+1));
	MAPNODES mnodes;

	LCSinit(X, Y, lookup, mnodes);
	LCSprint(X, Y, lookup);
	int lcs = lookup[M][N];
	cout << "Dlugosc: " << lcs << endl;
	VSOL res=LCSgetAll(X, Y, mnodes, M, N, lcs+1);

	//
	for (int i = 0; i < res.size();i++) 
	{
		cout << "Variant " << i+1 << ":" << endl;
		for (int j = 0; j < res[i].s.length(); j++) 
		{
			cout << res[i].s[j] << " " << res[i].x[j]+1 << " " << res[i].y[j]+1 << endl;
		}
		cout << endl;
	}
	return 0;
}
