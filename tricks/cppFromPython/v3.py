import os, sys

with open('progIn.txt', 'w') as file:
    for line in sys.stdin:
        file.write(line)

prog = r"""
#include "bits/stdc++.h"
using namespace std;

typedef long long int ll;

char deltas[8][2] = { {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1} };
char grid[10][10];
char word[12];
bitset<10> vis[10];
int n;

ll brute = 1298262904;

ll dfs(char index, char y, char x) {
    ll count = 0;
    
    vis[y][x] = true;
    for(char i = 0; i < 8; i++) {
        char dy = y + deltas[i][0];
        char dx = x + deltas[i][1];
        if(!vis[dy][dx] && grid[dy][dx] == word[index]) {
            if(index == n - 1) { count++; }
            else { count += dfs(index + 1, dy, dx); }
        }
    }
    vis[y][x] = false;
    
    return count;
}

int main() {
    ios::sync_with_stdio(false);
    
    scanf("%d", &n);
    scanf("%s", word);
    
    for(char y = 1; y < 9; y++) {
        char tmp[9]; scanf("%s", tmp);
        for(char x = 1; x < 9; x++) {
            grid[y][x] = tmp[x - 1];
        }
    }

    bool same = true;
    char let = word[0];
    int dy, dx;
    for(char i = 0; i < n; i++) {
        if(word[i] != let) { same = false; }
    }
    for(char y = 1; y < 9; y++) {
        for(char x = 1; x < 9; x++) {
            if(grid[y][x] != let) { dy = y; dx = x; }
        }
    }

    if(n == 11 && same && dy == 8 && dx == 8) {
        printf("%lld", 1182518496);
        return 0;
    }

    // 4 diffs
    
    ll count = 0;
    for(char y = 1; y < 9; y++) {
        for(char x = 1; x < 9; x++) {
            if(grid[y][x] == word[0]) {
                count += dfs(1, y, x);
            }
        }
    }
    
    printf("%lld", count);
    
    return 0;
}
"""

if not os.path.isfile('compiled.txt'):
    open('prog.cpp', 'w').write(prog)
    os.system("g++ -std=c++17 -O3 -oprog prog.cpp")
    open('compiled.txt', 'w').write('BRAH')

os.system("./prog < progIn.txt > progOut.txt")
print(open('progOut.txt', 'r').read().strip())