import os, sys

with open('progIn.txt', 'w') as file:
    for line in sys.stdin:
        file.write(line)

prog = r"""
#include <cstring>
#include <string>
#include <bitset>
using namespace std;

int k;
char grid[10][20] = { { 0 }};
char buffer[20];
bitset<10> used[10];

void clearBitset() {
    used[0].reset();
    used[1].reset();
    used[2].reset();
    used[3].reset();
    used[4].reset();
    used[5].reset();
    used[6].reset();
    used[7].reset();
    used[8].reset();
    used[9].reset();
}


int deltaRow[] = { -1, -1, -1, 0, 0, 1, 1, 1};
int deltaCol[] = { -1,  0,  1,-1, 1,-1, 0, 1};
int dfs(int row, int col, int depth) {
    int total=0;
    for (int j=0; j < 8; ++j) {
        int x = row+deltaRow[j], y=col+deltaCol[j];
        if (grid[x][y] == buffer[depth] && !used[x][y]) {
            ++depth;
            if (depth == k) { ++total; }
            else {
                used[x][y] = true;
                total += dfs(x, y, depth);
                used[x][y] = false;
            }
            --depth;
        }
    }
    return total;
}

int main() {
    int n;
    if (1 != scanf("%d\n", &k)) { return 1; }
    if (!fgets(buffer, 16, stdin)) { return 1; }
    for (n=1; n <= 8; ++n) {
        if (!fgets(grid[n]+1, 18, stdin)) { return 1; }
        grid[n][9] = 0;
    }

    buffer[k] = 0;
    int total=0;
    for (int u=1; u <= 8; ++u) {
        for (int v=1; v <= 8; ++v) {
            if (grid[u][v] == buffer[0]) {
                if (1 == k) { ++total; }
                else {
                    clearBitset();
                    used[u][v] = true;
                    total += dfs(u,v,1);
                    used[u][v] = false;
                }
            }
        }
    }
    printf("%d\n", total);
    return 0;
}

"""

if not os.path.isfile('compiled.txt'):
    open('prog.cpp', 'w').write(prog)
    os.system("g++ -std=c++17 -O3 -oprog prog.cpp")
    open('compiled.txt', 'w').write('BRAH')

os.system("./prog < progIn.txt > progOut.txt")
print(open('progOut.txt', 'r').read().strip())